<?php

use Illuminate\Support\Facades\Route;

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::resource('interviews', 'CandidatesController')->middleware('auth');

Route::get('candidates/deleteuser/{cid}', 'CandidatesController@destroy')->name('candidate.deleteuser');
Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');
Route::get('/user.candidates', 'CandidatesController@candidateBelongUser')->name('user.belong.candidates')->middleware('auth');
Route::get('/mycandidates', 'CandidatesController@myCandidates')->name('candidate.mycandidates')->middleware('auth');

Route::resource('users', 'UsersController')->middleware('auth');
Route::get('/users.index', 'UsersController@index')->name('user.show')->middleware('auth');

Route::get('deleteuser/{uid}', 'UsersController@destroy')->name('user.delete')->middleware('auth');
Route::get('edituser/{uid}', 'UsersController@edit')->name('users.edit')->middleware('auth');
Route::post('updateuser/{uid}', 'UsersController@update')->name('users.update')->middleware('auth');

Route::get('showuser/{uid}', 'UsersController@show')->name('users.show')->middleware('auth');
Route::post('changedepartmentuser/}', 'UsersController@changedepartment')->name('user.changedepartment')->middleware('auth');
Route::get('makemanager/{uid}', 'UsersController@makemanager')->name('makemanager')->middleware('auth');
Route::get('deletemanager/{uid}', 'UsersController@deletemanager')->name('deletemanager')->middleware('auth');


Route::get('/', function () {
    return view('welcome');
});
/* 
Route::get('/hello', function(){
    return 'Hello LARAVEL';
});

Route::get('/student/{id}', function($id = 'Student'){
    return 'We got stident with id:'.$id ;
});

Route::get('/car/{id?}', function($id = null){
    if (isset($id)){
        //TODO: validation for integer 
        return "we got car $id";
    }
    else {
        return 'we need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});



//EX 05
Route::get('/users/{email}/{name?}', function ($email = null, $name ='name missing') {
    return view('users',compact('email','name'));
});
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
