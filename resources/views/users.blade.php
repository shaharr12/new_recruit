<!DOCTYPE html>
    <head>

        <title>EX 05</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                color: black;
                font-weight: 900;

                font-family: 'Nunito', sans-serif;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

        </style>
    </head>
    <body>
            <div class="content">
                <div class="title">
                    Your Name Is: {{$name}} </br>
                    And Your Email Is: {{$email}}
                </div>
            </div>
    </body>
</html>
