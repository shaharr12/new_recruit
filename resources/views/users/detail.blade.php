@extends('layouts.app')

@section('title', 'User')

@section('content')
@if(Session::has('notallowd'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowd')}}
</div>
@endif




<h1>User details</h1>
<table class = "table table-dark">
    <!-- the table data -->
        <tr>
            <td>Id</td><td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td><td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td><td>{{$user->email}}</td>
        </tr>

        <tr>
            <td>Role</td><td>
            @foreach ($user->roles as $role)
                          <option value="{{ $role->id }}"> 
                              {{ $role->name }} 
                          </option>
                          @endforeach 
                          @can('assign-user1')
 
@if($role->name != 'manager'){
                          <a  class="btn btn-success btn-rounded btn-sm my-0" href= "{{route ('makemanager', $user->id)}}" >Make Manager</a> 

                          @endcan
}
@endif
                          @can('assign-user')
                          <a  class="btn btn-danger btn-rounded btn-sm my-0" href= "{{route ('deletemanager', $user->id)}}" >Delete Manager</a> 

                          @endcan

            
            </td>
        </tr>
       


        <tr>
        <td>
        Department
        </td>
        @can('assign-user1')
        <td>

        <form method="POST" action="{{ route('user.changedepartment') }}">
                    @csrf  
                    <div class="">
                    <label for="department_id" class="col-md-4 col-form-label "></label>
                    <div class="col-md-6">
                        <select class="form-control" name="department_id">                                                                         
                          @foreach ($departments as $department)
                          <option value="{{ $department->id }}"> 
                              {{ $department->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change Department
                                </button>
                            </div>
                    </div>                    
                </form> 
                @else
                Department</td><td>{{$user->department->name}}
                @endcan

</td>
        </tr> 


  
        
        <tr>
           <td>Created</td><td>{{$user->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$user->updated_at}}</td>  
        </tr>    
        </table>
                                                        





               
                </div>

@endsection
