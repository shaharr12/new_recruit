@extends('layouts.app')

@section('title', 'users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

@if(Session::has('success'))
<div class = 'alert alert-success'>
    {{Session::get('success')}}
</div>
@endif


<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Edit</th><th>Delete</th><th>Details</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>       
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td> 
            <td>
               <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>   
            <td>
                 <a href = "{{route('users.show',$user->id)}}">Info</a>
            </td>                                                                                                      
        </tr>
    @endforeach
</table>
@endsection
