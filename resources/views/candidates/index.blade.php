@extends('layouts.app')
@section('title', 'Main Page')
@section('content')

@if(Session::has('notallowd'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowd')}}
</div>
@endif



<div><a class="btn btn-light" role="button" href="{{url('/candidates/create')}}">Add New Condidate</a></div>

        
                               
       
        <br>
        <h1> List of Candidates</h1>
        
        <table class = 'table'>
        @csrf

        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>User</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
        </thead>

            <!-- the table data -->
            @foreach($candidates as $candidate)


                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>
                <div class="dropdown">
                @if(App\Status::next($candidate->status_id) !=NULL )

                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->status_id))
                            {{$candidate->status->name}}

                          @endif                    
                          </button>
                        @else
                        {{$candidate->status->name}}

                        @endif                    

                          @if(App\Status::next($candidate->status_id) !=NULL )
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           
                           
                        </div>
                        @endif                    
                    </div>
                </td>

                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->user_id))
                            {{$candidate->user->name}}
                           
                        @else
                        Assign owner
                        @endif                   
                         </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user)
                            <a class="dropdown-item" href= "{{route ('candidate.changeuser', [$candidate->id,$user->id])}}">{{$user->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>
            
                <td><a href= "{{action ('CandidatesController@edit', $candidate->id)}}" >edit</a></td>
                <td><a href= "{{route ('candidate.deleteuser', $candidate->id)}}">delete</a></td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
            </tr>
            @endforeach
            
        </table>


        
@endsection

