<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Candidate extends Model
{
    protected $fillable   = ['name', 'email']; //<---- Add this line for colonmes guarded/fillable for mass update, update (request->all)

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }
   
    public function interview(){
        return $this->belongsTo('App\Interview');
    }

   

}

