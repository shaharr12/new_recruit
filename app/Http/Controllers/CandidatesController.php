<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;





// full name is App\Http\Controllers\CandidatesController;
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();
        
        return view('candidates.index', compact('candidates','users','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    


    public function changeUser($cid,$uid = NULL)
    {
        Gate::authorize('assign-user',Auth::user());
        $candidate = Candidate::findOrFail($cid);
        $candidate-> user_id = $uid; 
        $candidate-> save();
        return redirect()->back();
    }

    public function changeStatus($cid,$sid )
    {
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status',$candidate)){
        $validation = Status::allowed($candidate->status_id, $sid);
        if($validation==TRUE){
            $candidate->status_id = $sid; 
            $candidate->save();
        }
    }
    else{
        Session::flash('notallowd','you cant');
    }
        return redirect('candidates');
      
}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate();
        $candidate -> name = $request->name; 
        $candidate -> email = $request->email;
        $candidate -> status_id = 1;
        $candidate -> save();
        return redirect('candidates');
    }

    public function candidateBelongUser()
    {   
        $user_id = Auth::id(); 
        $candidates = Candidate::where('user_id', $user_id)->get();
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        return view('candidates.edit' , compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $candidate = Candidate::findOrFail($id);
       $candidate -> update($request->all());
       return redirect('candidates'); 
    }
//       $candidate -> update($request->except(['_token','submit']));

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete(); 
        return redirect('candidates');
    }

    public function myCandidates()
    {   
        $user_id = Auth::id(); #the one that logged in
        $candidates = Candidate::where('user_id', $user_id)->get(); 
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses'));

}
}