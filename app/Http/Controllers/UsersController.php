<?php

namespace App\Http\Controllers;
use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use App\Userrole;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();

        return view('users.detail', compact('user','departments'));    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('assign-user1',Auth::user());
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.edit' , compact('user','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
       $user -> update($request->all());
       return redirect('users.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('assign-user1',Auth::user());
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users');
    }

    public function changedepartment(Request $request){


        $uid = $request->id;
        $did = $request->department_id;
        $user = User::findOrFail($uid);
        $candidates =  $user->candidate;
        if($candidates->isEmpty()){
            $user->department_id = $did;
            $user->save();
        }
        else{
            Session::flash('notallowd','this user still have some candidates');
        }
        
        return redirect()->back();

    }

    public function makemanager($id)
    {
        $role = new Userrole();
        $role->user_id = $id;
        $role->role_id = 2;
        $role->save();
        Session::flash('success','change to manager Successfully');

        return redirect('users.index');   
    
    }


        public function deletemanager($id)
        {

            DB::table('userroles')->where('user_id', $id)->where('role_id', 2)->delete();
            return redirect('users.index');    }

}
